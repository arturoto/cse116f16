package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;
import gamepieces.ClueCharacter;
import gamepieces.Room;
import gamepieces.Weapon;
import individuals.Players;

public class provingFalseSuggestion {

	ArrayList<Players> plays = new ArrayList<Players>();
	Weapon w = new Weapon();
	Room r = new Room();
	ClueCharacter c = new ClueCharacter();
	Players sugg = new Players(w, r, c);
	//sugg needs to be whatever suggestion in the game
	
	//* Suggestion would be answered by the next player because they have the Player card
	@Test
	public void correctPlayerTest() {
		Weapon a = null;
		Room b = null;
		ClueCharacter c = null;
		Players nextPlay = new Players(a, b, c);
		assertEquals (nextPlay.getCharacterCard(), null);
	}
	
	//* Suggestion would be answered by the next player because they have the Room card
	@Test
	public void correctRoomTest(){
		Weapon a = null;
		Room b = null;
		ClueCharacter c = null;
		Players nextPlay = new Players (a, b, c);
		assertEquals(nextPlay.getRoomCard(), null);
	}
	
	//* Suggestion would be answered by the next player because they have the Weapon card
	@Test
	public void correctWeaponTest (){
		Weapon a = null;
		Room b = null;
		ClueCharacter c = null;
		Players nextPlay = new Players (a, b, c);
		assertEquals (nextPlay.getWeaponCard(), null);
	}
	
	//* Suggestion would be answered by the next player because they have 2 matching cards;
	@Test
	public void twoCorrectCardsTest(){
		Weapon a = null;
		Room b = null;
		ClueCharacter c = null;
		Players nextPlay = new Players (a, b, c);
		boolean two;
		boolean boo = (nextPlay.getCharacterCard() == sugg.getCharacterCard());
		boolean baa = (nextPlay.getRoomCard() == sugg.getRoomCard());
		boolean bee = (nextPlay.getWeaponCard() == sugg.getWeaponCard());
		if((boo && bee) || (boo && baa) || (baa && bee)){
			two = true;
		}
		else{
			two = false;
		}
		assertFalse(two);
	}
	
	//* Suggestion would be answered by the player after the next player because they have 1 or more matching cards;
	@Test
	public void oneMoreCorrectCardsTest(){
		Weapon a = null;
		Room b = null;
		ClueCharacter c = null;
		Players nextPlay = new Players (a, b, c);
		boolean boo = (nextPlay.getCharacterCard() == sugg.getCharacterCard());
		boolean baa = (nextPlay.getRoomCard() == sugg.getRoomCard());
		boolean bee = (nextPlay.getWeaponCard() == sugg.getWeaponCard());
		boolean all = (boo || baa || bee || ((boo && bee) || (boo && baa) || (baa && bee)) || (baa && boo && bee));
		assertFalse (all);
	}
	
	//madeSuggestion will be the cards of the person who is making the suggestion
	
	//* Suggestion cannot be answered by any player but the player making the suggestion has 1 or more matching cards;
	@Test
	public void hasSuggestionCard (){
		Weapon a = null;
		Room b = null;
		ClueCharacter c = null;
		Players madeSuggestion = new Players (a, b, c);
		boolean boo = (sugg.getCharacterCard() == madeSuggestion.getCharacterCard());
		boolean baa = (sugg.getRoomCard() == madeSuggestion.getRoomCard());
		boolean bee = (sugg.getWeaponCard() == madeSuggestion.getWeaponCard());
		assertFalse (boo || baa || bee);
	}
	
	// *Suggestion cannot be answered by any player and the player making the suggestion does not have any matching cards.
	@Test
	public void noSuggestionCard (){
		Weapon a = null;
		Room b = null;
		ClueCharacter c = null;
		Players madeSuggestion = new Players (a, b, c);
		boolean boo = (sugg.getCharacterCard() == madeSuggestion.getCharacterCard());
		boolean baa = (sugg.getRoomCard() == madeSuggestion.getRoomCard());
		boolean bee = (sugg.getWeaponCard() == madeSuggestion.getWeaponCard());
		assertFalse (boo && baa && bee);
	}

}

