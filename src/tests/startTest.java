package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import individuals.ColonelMustard;
import individuals.MissScarlet;
import individuals.MrGreen;
import individuals.MrsPeacock;
import individuals.MrsWhite;
import individuals.ProfessorPlum;

public class startTest {

	ColonelMustard _cm = new ColonelMustard();
	MissScarlet _ms = new MissScarlet();
	ProfessorPlum _pp = new ProfessorPlum();
	MrGreen _mg = new MrGreen();
	MrsWhite _mw = new MrsWhite();
	MrsPeacock _mp = new MrsPeacock();
	
	//start test allows only Miss Scarlet to take the first turn, not any other player
	@Test
	public void starttest() {
		boolean f = _ms.checkYourself();
		assertTrue (f);
	}
	
	@Test
	public void nonStarttest(){
		boolean nf = _cm.checkYourself();
		assertFalse (nf);
	}
	
	@Test
	public void nonStartTestOne(){
		boolean nf = _pp.checkYourself();
		assertFalse(nf);
	}
	
	@Test
	public void nonStartTestTwo(){
		boolean nf = _mg.checkYourself();
		assertFalse(nf);
	}
	
	@Test
	public void nonStartTestThree(){
		boolean nf = _mw.checkYourself();
		assertFalse(nf);
	}
	
	@Test
	public void nonStartTestFour(){
		boolean nf = _mp.checkYourself();
		assertFalse(nf);
	}

}