package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import gamepieces.Movement;
import gamepieces.Board;

public class throughdoorTest {

	@Test
	public void test() {
		boolean result;
		Board move = new Board();
		boolean room0 = move.conservatory();
		boolean room1 = move.ballroom();
		boolean room2 = move.kitchen();
		boolean room3 = move.diningRoom();
		boolean room4 = move.lounge();
		boolean room5 = move.hall();
		boolean room6 = move.library();
		if(room0 == true || room1 == true || room2 == true || room3 == true || room4 == true || room5 == true || room6 == true){
			result = true;
		}
		else{
			result = false;
		}
		assertTrue(result);
	}	
}
