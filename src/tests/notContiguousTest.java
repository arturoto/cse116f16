package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import gamepieces.ClueCharacter;
import gamepieces.Dice;
import gamepieces.Movement;

public class notContiguousTest {
	
	ClueCharacter _cc;

	@Test
	public void test() {
		Dice d = new Dice();
		int dieNum = d.roll();
		boolean resultV;
		boolean resultH;
		boolean resultVH;
		Movement move = new Movement();
		boolean verTotal = move.Vertical(_cc, dieNum, dieNum, null);
		if(verTotal == true){
			resultV = false;
		}
		else{
			resultV = true;
		}
		boolean horTotal = move.Horizontal(_cc, dieNum, dieNum, null);
		if(horTotal == true){
			resultH = false;
		}
		else{
			resultH = true;
		}
		boolean bothTotal = move.vertAndHorz(_cc, null);
		if(bothTotal == true){
			resultVH = false;
		}
		else{
			resultVH = true;
		}
		assertFalse(resultV);
		assertFalse(resultH);
		assertFalse(resultVH);
	}
}


