package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import gamepieces.Dice;

public class randomDiceTest {

	@Test
	public void diceTest() {
		Dice d = new Dice();
		int first = d.roll();
		int second = d.roll();
		System.out.println(first + ", " + second);
		assertNotEquals(first, second);
	}

}
