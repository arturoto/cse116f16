package individuals;

import java.util.ArrayList;
import java.util.Iterator;
import gamepieces.ClueCharacter;
import gamepieces.Room;
import gamepieces.Weapon;
import gamepieces.Board;

public class Players {
	
	/*
	 * Each player needs to pick a number to decide the order they can "suggest" later
	 */
	
	//methods that return weapon card, room card, and character card
	
	Weapon w = new Weapon();
	Room r = new Room();
	ClueCharacter c = new ClueCharacter();
	
	public Players (Weapon weapon, Room room, ClueCharacter character){
		w = weapon;
		r = room;
		c = character;
	}
	
	//right now, getWeaponCard, getRoomCard, getCharacterCard, suggestedWeapon, suggestedRoom, and suggestedCharacter
	//will all return the same results, but eventually after making some of the user-oriented code, we will be able
	//to make these return whatever button the player clicked
	
	public Weapon getWeaponCard(){
		return w;
	}
	
	public Room getRoomCard(){
		return r;
	}
	
	public ClueCharacter getCharacterCard(){
		return c;
	}
	
	public int getPositionOnBoard(int i, int j){
		Board b = new Board();
		int position = b.positionOnBoard(i,j);
		return position;
	}
	public Iterator<ArrayList> iter(){
		Iterator it = null;
		return it;
	}
	
	//will need to return the suggested weapon
	public Weapon suggestedWeapon(){
		return w;
	}
	
	//will need to return the suggested room
	public Room suggestedRoom(){
		return r;
	}

	//will need to return the suggested charcter
	public ClueCharacter suggestedCharacter(){
		return c;
	}
	public boolean suggest(){
		//needs to match suggestions to the right answer and return true or false value
		return true;
	}
}
