package gamepieces;

public class Board {
	int [][] _location;

	int[][] _spaces = new int[25][25];
	//public void placesOnBoard (){
		
		//entire board minus the complete outline of squares
		//int[][] _spaces = new int[24][24];
		
		public boolean conservatory(){
		//spaces that constitute the conservatory room - all will hold "1"
		_spaces[2][1] = 1;
		_spaces[2][2] = 1;
		_spaces[2][3] = 1;
		_spaces[2][4] = 1;
		_spaces[2][5] = 1;
		_spaces[2][6] = 1;
		_spaces[3][1] = 1;
		_spaces[3][2] = 1;
		_spaces[3][3] = 1;
		_spaces[3][4] = 1;
		_spaces[3][5] = 1;
		_spaces[3][6] = 1;
		_spaces[4][1] = 1;
		_spaces[4][2] = 1;
		_spaces[4][3] = 1;
		_spaces[4][4] = 1;
		_spaces[4][5] = 1;
		_spaces[4][6] = 1;
		_spaces[5][2] = 1;
		_spaces[5][3] = 1;
		_spaces[5][4] = 1;
		_spaces[5][5] = 1;
		return true;
		}
		
		public boolean ballroom(){
		//spaces that constitute the ballroom - all will hold "2"
		_spaces[2][11] = 2;
		_spaces[2][12] = 2;
		_spaces[2][13] = 2;
		_spaces[2][14] = 2;
		_spaces[3][9] = 2;
		_spaces[3][10] = 2;
		_spaces[3][11] = 2;
		_spaces[3][12] = 2;
		_spaces[3][13] = 2;
		_spaces[3][14] = 2;
		_spaces[3][15] = 2;
		_spaces[3][16] = 2;
		_spaces[4][9] = 2;
		_spaces[4][10] = 2;
		_spaces[4][11] = 2;
		_spaces[4][12] = 2;
		_spaces[4][13] = 2;
		_spaces[4][14] = 2;
		_spaces[4][15] = 2;
		_spaces[4][16] = 2;
		_spaces[5][9] = 2;
		_spaces[5][10] = 2;
		_spaces[5][11] = 2;
		_spaces[5][12] = 2;
		_spaces[5][13] = 2;
		_spaces[5][14] = 2;
		_spaces[5][15] = 2;
		_spaces[5][16] = 2;
		_spaces[6][9] = 2;
		_spaces[6][10] = 2;
		_spaces[6][11] = 2;
		_spaces[6][12] = 2;
		_spaces[6][13] = 2;
		_spaces[6][14] = 2;
		_spaces[6][15] = 2;
		_spaces[6][16] = 2;
		_spaces[7][9] = 2;
		_spaces[7][10] = 2;
		_spaces[7][11] = 2;
		_spaces[7][12] = 2;
		_spaces[7][13] = 2;
		_spaces[7][14] = 2;
		_spaces[7][15] = 2;
		_spaces[7][16] = 2;
		_spaces[8][9] = 2;
		_spaces[8][10] = 2;
		_spaces[8][11] = 2;
		_spaces[8][12] = 2;
		_spaces[8][13] = 2;
		_spaces[8][14] = 2;
		_spaces[8][15] = 2;
		_spaces[8][16] = 2;
		return true;
		}
		
		public boolean kitchen(){
		//spaces that constitute the kitchen - all spaces will hold "3"
		_spaces[2][19] = 3;
		_spaces[2][20] = 3;
		_spaces[2][21] = 3;
		_spaces[2][22] = 3;
		_spaces[2][23] = 3;
		_spaces[2][24] = 3;
		_spaces[3][19] = 3;
		_spaces[3][20] = 3;
		_spaces[3][21] = 3;
		_spaces[3][22] = 3;
		_spaces[3][23] = 3;
		_spaces[3][24] = 3;
		_spaces[4][19] = 3;
		_spaces[4][20] = 3;
		_spaces[4][21] = 3;
		_spaces[4][22] = 3;
		_spaces[4][23] = 3;
		_spaces[4][24] = 3;
		_spaces[5][19] = 3;
		_spaces[5][20] = 3;
		_spaces[5][21] = 3;
		_spaces[5][22] = 3;
		_spaces[5][23] = 3;
		_spaces[5][24] = 3;
		_spaces[6][19] = 3;
		_spaces[6][20] = 3;
		_spaces[6][21] = 3;
		_spaces[6][22] = 3;
		_spaces[6][23] = 3;
		_spaces[6][24] = 3;
		_spaces[7][19] = 3;
		_spaces[7][20] = 3;
		_spaces[7][21] = 3;
		_spaces[7][22] = 3;
		_spaces[7][23] = 3;
		return true;
		}
		
		public boolean diningRoom(){
		//these spaces constitute the dining room - all will hold "4"
		_spaces[10][20] = 4;
		_spaces[10][21] = 4;
		_spaces[10][22] = 4;
		_spaces[10][23] = 4;
		_spaces[10][24] = 4;
		_spaces[11][17] = 4;
		_spaces[11][18] = 4;
		_spaces[11][19] = 4;
		_spaces[11][20] = 4;
		_spaces[11][21] = 4;
		_spaces[11][22] = 4;
		_spaces[11][23] = 4;
		_spaces[11][24] = 4;
		_spaces[12][17] = 4;
		_spaces[12][18] = 4;
		_spaces[12][19] = 4;
		_spaces[12][20] = 4;
		_spaces[12][21] = 4;
		_spaces[12][22] = 4;
		_spaces[12][23] = 4;
		_spaces[12][24] = 4;
		_spaces[13][17] = 4;
		_spaces[13][18] = 4;
		_spaces[13][19] = 4;
		_spaces[13][20] = 4;
		_spaces[13][21] = 4;
		_spaces[13][22] = 4;
		_spaces[13][23] = 4;
		_spaces[13][24] = 4;
		_spaces[14][17] = 4;
		_spaces[14][18] = 4;
		_spaces[14][19] = 4;
		_spaces[14][20] = 4;
		_spaces[14][21] = 4;
		_spaces[14][22] = 4;
		_spaces[14][23] = 4;
		_spaces[14][24] = 4;
		_spaces[15][17] = 4;
		_spaces[15][18] = 4;
		_spaces[15][19] = 4;
		_spaces[15][20] = 4;
		_spaces[15][21] = 4;
		_spaces[15][22] = 4;
		_spaces[15][23] = 4;
		_spaces[15][24] = 4;
		_spaces[16][17] = 4;
		_spaces[16][18] = 4;
		_spaces[16][19] = 4;
		_spaces[16][20] = 4;
		_spaces[16][21] = 4;
		_spaces[16][22] = 4;
		_spaces[16][23] = 4;
		_spaces[16][24] = 4;
		return true;
		}
		
		public boolean lounge(){
		//these spaces constitute the lounge - they will hold '5'
		_spaces[19][18] = 5;
		_spaces[19][19] = 5;
		_spaces[19][20] = 5;
		_spaces[19][21] = 5;
		_spaces[19][22] = 5;
		_spaces[19][23] = 5;
		_spaces[19][24] = 5;
		_spaces[20][18] = 5;
		_spaces[20][19] = 5;
		_spaces[20][20] = 5;
		_spaces[20][21] = 5;
		_spaces[20][22] = 5;
		_spaces[20][23] = 5;
		_spaces[20][24] = 5;
		_spaces[21][18] = 5;
		_spaces[21][19] = 5;
		_spaces[21][20] = 5;
		_spaces[21][21] = 5;
		_spaces[21][22] = 5;
		_spaces[21][23] = 5;
		_spaces[21][24] = 5;
		_spaces[22][18] = 5;
		_spaces[22][19] = 5;
		_spaces[22][20] = 5;
		_spaces[22][21] = 5;
		_spaces[22][22] = 5;
		_spaces[22][23] = 5;
		_spaces[22][24] = 5;
		_spaces[23][18] = 5;
		_spaces[23][19] = 5;
		_spaces[23][20] = 5;
		_spaces[23][21] = 5;
		_spaces[23][22] = 5;
		_spaces[23][23] = 5;
		_spaces[23][24] = 5;
		_spaces[24][19] = 5;
		_spaces[24][20] = 5;
		_spaces[24][21] = 5;
		_spaces[24][22] = 5;
		_spaces[24][23] = 5;
		_spaces[24][24] = 5;
		return true;
		}
		
		public boolean hall(){
		//these spaces constitute the hall - they hold "6"
		_spaces[10][18] = 6;
		_spaces[10][19] = 6;
		_spaces[10][20] = 6;
		_spaces[10][21] = 6;
		_spaces[10][22] = 6;
		_spaces[10][23] = 6;
		_spaces[11][18] = 6;
		_spaces[11][19] = 6;
		_spaces[11][20] = 6;
		_spaces[11][21] = 6;
		_spaces[11][22] = 6;
		_spaces[11][23] = 6;
		_spaces[12][18] = 6;
		_spaces[12][19] = 6;
		_spaces[12][20] = 6;
		_spaces[12][21] = 6;
		_spaces[12][22] = 6;
		_spaces[12][23] = 6;
		_spaces[13][18] = 6;
		_spaces[13][19] = 6;
		_spaces[13][20] = 6;
		_spaces[13][21] = 6;
		_spaces[13][22] = 6;
		_spaces[13][23] = 6;
		_spaces[14][18] = 6;
		_spaces[14][19] = 6;
		_spaces[14][20] = 6;
		_spaces[14][21] = 6;
		_spaces[14][22] = 6;
		_spaces[14][23] = 6;
		_spaces[15][18] = 6;
		_spaces[15][19] = 6;
		_spaces[15][20] = 6;
		_spaces[15][21] = 6;
		_spaces[15][22] = 6;
		_spaces[15][23] = 6;
		return true;
		}
		
		public boolean study(){
		//these spaces constitute the study - they will hold "7"
		_spaces[21][1] = 7;
		_spaces[21][2] = 7;
		_spaces[21][3] = 7;
		_spaces[21][4] = 7;
		_spaces[21][5] = 7;
		_spaces[21][6] = 7;
		_spaces[21][7] = 7;
		_spaces[22][1] = 7;
		_spaces[22][2] = 7;
		_spaces[22][3] = 7;
		_spaces[22][4] = 7;
		_spaces[22][5] = 7;
		_spaces[22][6] = 7;
		_spaces[22][7] = 7;
		_spaces[23][1] = 7;
		_spaces[23][2] = 7;
		_spaces[23][3] = 7;
		_spaces[23][4] = 7;
		_spaces[23][5] = 7;
		_spaces[23][6] = 7;
		_spaces[23][7] = 7;
		_spaces[24][1] = 7;
		_spaces[24][2] = 7;
		_spaces[24][3] = 7;
		_spaces[24][4] = 7;
		_spaces[24][5] = 7;
		_spaces[24][6] = 7;
		return true;
		}
		
		public boolean library(){ 
		//these spaces constitute the library - they will hold "8"
		_spaces[16][1] = 8;
		_spaces[16][2] = 8;
		_spaces[16][3] = 8;
		_spaces[16][4] = 8;
		_spaces[16][5] = 8;
		_spaces[16][6] = 8;
		_spaces[16][7] = 8;
		_spaces[17][1] = 8;
		_spaces[17][2] = 8;
		_spaces[17][3] = 8;
		_spaces[17][4] = 8;
		_spaces[17][5] = 8;
		_spaces[17][6] = 8;
		_spaces[17][7] = 8;
		_spaces[18][1] = 8;
		_spaces[18][2] = 8;
		_spaces[18][3] = 8;
		_spaces[18][4] = 8;
		_spaces[18][5] = 8;
		_spaces[18][6] = 8;
		_spaces[18][7] = 8;
		_spaces[15][2] = 8;
		_spaces[15][3] = 8;
		_spaces[15][4] = 8;
		_spaces[15][5] = 8;
		_spaces[15][6] = 8;
		_spaces[19][2] = 8;
		_spaces[19][3] = 8;
		_spaces[19][4] = 8;
		_spaces[19][5] = 8;
		_spaces[19][6] = 8;
		return true;
		}
		
		public boolean billardRoom(){
		//these spaces constitute billiard room - they will hold "9"
		_spaces[9][1] = 9;
		_spaces[9][2] = 9;
		_spaces[9][3] = 9;
		_spaces[9][4] = 9;
		_spaces[9][5] = 9;
		_spaces[9][6] = 9;
		_spaces[10][1] = 9;
		_spaces[10][2] = 9;
		_spaces[10][3] = 9;
		_spaces[10][4] = 9;
		_spaces[10][5] = 9;
		_spaces[10][6] = 9;
		_spaces[11][1] = 9;
		_spaces[11][2] = 9;
		_spaces[11][3] = 9;
		_spaces[11][4] = 9;
		_spaces[11][5] = 9;
		_spaces[11][6] = 9;
		_spaces[12][1] = 9;
		_spaces[12][2] = 9;
		_spaces[12][3] = 9;
		_spaces[12][4] = 9;
		_spaces[12][5] = 9;
		_spaces[12][6] = 9;
		_spaces[13][1] = 9;
		_spaces[13][2] = 9;
		_spaces[13][3] = 9;
		_spaces[13][4] = 9;
		_spaces[13][5] = 9;
		_spaces[13][6] = 9;
		return true;
		}
		
		public boolean innerHallWay(){
		//these spaces are empty - they are outside any room or inner-hallway - they will hold "0"
		_spaces[14][1] = 0;
		_spaces[15][1] = 0;
		_spaces[1][1] = 0;
		_spaces[1][2] = 0;
		_spaces[1][3] = 0;
		_spaces[1][4] = 0;
		_spaces[1][5] = 0;
		_spaces[1][6] = 0;
		_spaces[1][7] = 0;
		_spaces[1][8] = 0;
		_spaces[1][9] = 0;
		_spaces[1][11] = 0;
		_spaces[1][12] = 0;
		_spaces[1][13] = 0;
		_spaces[1][14] = 0;
		_spaces[1][16] = 0;
		_spaces[1][17] = 0;
		_spaces[1][18] = 0;
		_spaces[1][19] = 0;
		_spaces[1][20] = 0;
		_spaces[1][21] = 0;
		_spaces[1][22] = 0;
		_spaces[1][23] = 0;
		_spaces[1][24] = 0;
		_spaces[2][7] = 0;
		_spaces[2][18] = 0;
		_spaces[7][24] = 0;
		_spaces[9][24] = 0;
		_spaces[6][1] = 0;
		_spaces[8][1] = 0;
		_spaces[18][24] = 0;
		_spaces[16][24] = 0;
		return false;
		}
		
		public boolean startSpaces(){
		//these spaces are starting spaces - they will hold "10"
		_spaces[17][24] = 10;
		_spaces[1][10] = 10;
		_spaces[1][15] = 10;
		_spaces[7][1] = 10;
		_spaces[19][1] = 10;
		_spaces[24][17] = 10;
		return false;
		}
	
		public boolean middleOfBoard(){
		//these spaces are the middle of the board, where the cards usually lie - they will hold "11"
		_spaces[11][10] = 11;
		_spaces[11][11]= 11;
		_spaces[11][12] = 11;
		_spaces[11][13] = 11;
		_spaces[11][14] = 11;
		_spaces[12][10] = 11;
		_spaces[12][11] = 11;
		_spaces[12][12] = 11;
		_spaces[12][13] = 11;
		_spaces[12][14] = 11;
		_spaces[13][10] = 11;
		_spaces[13][11] = 11;
		_spaces[13][12] = 11;
		_spaces[13][13] = 11;
		_spaces[13][14] = 11;
		_spaces[14][10] = 11;
		_spaces[14][11] = 11;
		_spaces[14][12] = 11;
		_spaces[14][13] = 11;
		_spaces[14][14] = 11;
		_spaces[15][10] = 11;
		_spaces[15][11] = 11;
		_spaces[15][12] = 11;
		_spaces[15][13] = 11;
		_spaces[15][14] = 11;
		_spaces[16][10] = 11;
		_spaces[16][11] = 11;
		_spaces[16][12] = 11;
		_spaces[16][13] = 11;
		_spaces[16][14] = 11;
		_spaces[17][10] = 11;
		_spaces[17][11] = 11;
		_spaces[17][12] = 11;
		_spaces[17][13] = 11;
		_spaces[17][14] = 11;
		return false;
		}
	
	public int positionOnBoard(int i, int j){
		int pos = _location[i][j];
		return pos;
	}

}
