package gamepieces;

public class ClueCharacter {
	
	protected ClueCharacter _cm;
	protected ClueCharacter _ms;
	protected ClueCharacter _mg;
	protected ClueCharacter _mp;
	protected ClueCharacter _mw;
	protected ClueCharacter _pp;
	
	public ClueCharacter (){
		String firstch = "Miss Scarlet";
		String secondch = "Colonel Mustard";
		String thirdch = "Mr Green";
		String fourthch = "Mrs Peacock";
		String fifthch = "Mrs White";
		String sixthch = "Professor Plum";
	}
	
	public boolean getCharacter(ClueCharacter cm, ClueCharacter ms, ClueCharacter mg, ClueCharacter mp, ClueCharacter mw, ClueCharacter pp){
		_cm = cm;
		_ms = ms;
		_mg = mg;
		_mp = mp;
		_mw = mw;
		_pp = pp;
		return true;
	}
	
}
