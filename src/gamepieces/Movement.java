package gamepieces;

import gamepieces.Board;

public class Movement {
	
	ClueCharacter _car;
	int[][] _location;
	
	public int[][] positionOnBoard(int i, int j){
		i = 1;
		j = 1;
		int[][] pos = new int[i][j];
		return pos;
	}
	public boolean horzMove(boolean result, int[][] location, ClueCharacter cc){
		_location = location;
		int i = 0;
		int j = 0;
		_car = cc;
		if(Horizontal(cc, i, j, location)){
			result = true;
		}
		else{
			result = false;
		}
		return result;
	}
	public boolean vertMove(boolean result, int[][] location, ClueCharacter cc){
		_location = location;
		int i = 0;
		int j = 0;
		_car = cc;
		if(Vertical(cc, i, j, location)){
			result = true;
		}
		else{
			result = false;
		}
		return result;
	}
	
	public boolean Horizontal(ClueCharacter cc, int i, int j, int[][] location){
		_location = location;
		int[][] pos = positionOnBoard(i, j);
		Dice d = new Dice();
		int num = d.roll();
		int counter = 0;
		_car = cc;
		while(counter < num){
			j += 1;
			counter++;
		}
		return true;
		
	}
	public boolean Vertical(ClueCharacter cc, int i, int j, int[][] location){
		_location = location;
		int pos[][] = positionOnBoard(i, j);
		Dice d = new Dice();
		int num = d.roll();
		int counter = 0;
		_car = cc;
		while(counter < num){
			i += 1;
			counter++;
		}
		return true;
	}
	public boolean vertAndHorz(ClueCharacter cc, int[][] location){
		_location = location;
		int i = 0;
		int j = 0;
		boolean vert = false;
		boolean horz = false;
		int pos[][] = positionOnBoard(i, j);
		Dice d = new Dice();
		int num = d.roll();
		int counter = 0;
		_car = cc;
		while(counter < num){
			if(horzMove(horz, location, cc)){
				j += 1;
			}
			if(vertMove(vert, location, cc)){
				i += 1;
			}
			counter++;
		}
		return true;
		
	}

}
